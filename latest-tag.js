#!/usr/bin/env node
const yargs = require('yargs').argv;

const semver = require("semver");
const exec = require("child_process").execSync;

const variant = yargs.variant;
if(!variant) {
    console.error("Usage: latest-tag --variant [variant]");
    process.exit(1)
}

const outputMethod = yargs.outputMethod || 'json';

const tagRegex = /([a-z]+)-(\d+.\d+.\d+)(?:-(?:(\d+)))*/;
const tagsOutput = exec("git tag").toString();

if(tagsOutput.length === 0) {
	console.error(`No tags to process. Make sure you're executing in a git repo.`);
	process.exit(0);
}

const tagData = tagsOutput.split(/\r?\n/)
	.filter(repositoryTag => tagRegex.test(repositoryTag))
	.map(repositoryTag => {
	const [ tag, variant, version, buildNumber=0 ] = tagRegex.exec(repositoryTag);
	return { tag, variant, version, buildNumber}
});

const latestTagData = tagData
	.filter(tagData => tagData.variant === variant)
	.reduce((latestTagData, currentTagData, index) => {

		if(index === 0) return currentTagData;

		const { version: latestVersion, buildNumber: latestBuildNumber=1 } = latestTagData;
		const { version: currentVersion, buildNumber: currentBuildNumber=1 } = currentTagData;

		const versionGreater = semver.gt(currentVersion, latestVersion);
		const builderNumberGreater = (currentVersion === latestVersion) && (parseInt(currentBuildNumber, 10) > parseInt(latestBuildNumber, 10));


		return (versionGreater || builderNumberGreater) ? currentTagData : latestTagData;

	}, null);


if(!latestTagData) {
	console.error('Could not determine version.');
	process.exit(1);
}

switch(outputMethod) {
	case 'json':
		console.log(JSON.stringify(latestTagData));
		break;
	default:
		console.log(`export LATEST_TAG_NAME=${latestTagData.tag}`);
		console.log(`export LATEST_TAG_BUILD_NUMBER=${latestTagData.buildNumber}`);
		console.log(`export LATEST_TAG_VERSION=${latestTagData.version}`);
		console.log(`export LATEST_TAG_VARIANT=${latestTagData.variant}`);
		break;
}

process.exit(0);
