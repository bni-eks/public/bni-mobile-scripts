#!/usr/bin/env node
const exec = require("child_process").execSync;
const yargs = require('yargs').argv;

function getLatestTag() {
    const latestTagData = JSON.parse(exec(`latest-tag --variant ${variant}`).toString());
	return latestTagData.tag;
}

const variant = yargs.variant;

if(!variant) {
    console.error("Usage: detect-build-type --variant [variant]");
    process.exit(1)
}

const verbose = yargs.verbose;
const fromGitRef = yargs.fromRef || getLatestTag();

// to ref is not inclusive at the moment, until figure out workaround use HEAD
// const toGitRef = yargs.toRef || 'HEAD';
const toGitRef = 'HEAD';

if(verbose) {
	console.log('fromRef = ' + fromGitRef);
	console.log('toRef = ' + toGitRef);
}

const diffCommand = `git diff --name-only ${toGitRef} ${fromGitRef}^`;
if(verbose) console.log(diffCommand);

const diffSinceLastRelease = exec(diffCommand).toString().split(/\r?\n/);
if(verbose) console.log('diff output = ' + JSON.stringify(diffSinceLastRelease, null, 2));

const isNativeFile = (file) =>
	file.startsWith('.env.member') ||
	file.startsWith('.env.corporate') ||
	(file.startsWith('android') && !file.endsWith('sentry.properties'))||
	(file.startsWith('ios') && !file.endsWith('sentry.properties')) ||
	file.startsWith('yarn.lock') ||
	file.startsWith('package.json');

const sourceChangeList = diffSinceLastRelease.filter(diffFile => diffFile.startsWith('src'));
if(verbose) console.log('src changes = ' + JSON.stringify(sourceChangeList, null, 2));

const nativeChangeList = diffSinceLastRelease.filter(isNativeFile);
if(verbose) console.log('native changes = ' + JSON.stringify(nativeChangeList, null, 2));

if(nativeChangeList.length > 0) {
    console.log(`export BUILD_TYPE=native`);
    process.exit(0);
}

if(sourceChangeList.length > 0) {
    console.log(`export BUILD_TYPE=codepush`);
    process.exit(0);
}

process.exit(1);


