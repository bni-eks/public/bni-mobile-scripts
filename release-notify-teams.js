#!/usr/bin/env node
const phin = require('phin');
const yargs = require('yargs').argv;

const SUCCESS_HEX_THEME = '00B32C';
const ERROR_HEX_THEME = 'AA0114';

console.log('here');
const webhook = yargs.webhook;
const title = yargs.title;
const subtitle = yargs.subtitle || '';
const image = yargs.image;
const version = yargs.buildVersion;
const buildType = yargs.buildType;
const commit = yargs.commit;
const variant = yargs.variant;

if(!webhook) {
	console.error("--webhook argument is required");
	process.exit(1)
}

if(!title) {
	console.error("--title argument is required");
	process.exit(1)
}

if(!version) {
	console.error("--buildVersion argument is required");
	process.exit(1)
}

if(!buildType) {
	console.error("--buildType argument is required");
	process.exit(1)
}

if(!variant) {
	console.error("--variant argument is required");
	process.exit(1)
}

if(!commit) {
	console.error("--commit argument is required");
	process.exit(1)
}

const isNative = (buildType === 'native');
const success = yargs.success || true;

const factList = [
	{
		name: "Version",
		value: version
	},
	{
		name: "Variant",
		value: variant
	},
	{
		name: "Release Type",
		value: isNative ? 'Native' : 'Code Push'
	},
	{
		name: "Commit",
		value: commit
	}
];

const buildNumber = yargs.buildNumber ? parseInt(yargs.buildNumber) : null;

if(buildNumber && buildNumber !== 0 && buildType === 'codepush') {
	factList.push({
		name: "Build Number",
		value: buildNumber + 1
	});
}

const microsoftTeamsPayload = {
	"@type": "MessageCard",
	"@context": "http://schema.org/extensions",
	themeColor: success ? SUCCESS_HEX_THEME : ERROR_HEX_THEME,
	summary: title,
	sections: [{
		activityTitle: title,
		activitySubtitle: subtitle,
		activityImage: image,
		facts: factList,
		markdown: true
	}]
};

phin({
	url: webhook,
	method: 'POST',
	data: microsoftTeamsPayload
}).then(data => {
	console.log(`Sent`);
	process.exit(0);
}).catch(e => {
	console.log(`Error: ${e.message}`);
	process.exit(1);
});
